OBJ = main.o Array.o High_order_basic.o High_order_First.o High_order_FORCE.o High_order_2FORCE.o

CFLAGS    = -Wall -fsignaling-nans -O3
DFLAGS    = -DCOMPILING_HOSTNAME=\"$(HOSTNAME)\"
LFLAGS    =
LIBS =  -lm
CC   = g++
LINK = g++

all:	high_order_streamer

%.o:	%.cpp
	$(CC) $(CFLAGS) $(DFLAGS) $(INCLUDE_DIRS) -o $@ -c $<

%.o:	%.cc
	$(CC) $(CFLAGS) $(DFLAGS) $(INCLUDE_DIRS) -o $@ -c $<

high_order_streamer:  $(OBJ)
	$(LINK) $(LFLAGS) -std=gnu++0x $(OBJ) -o $@ $(LIBS)


clean:
	rm Input/Output/* high_order_streamer *.o 2> /dev/null
