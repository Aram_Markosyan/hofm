//
//  High_order_FORCE.cpp
//  High_order_fluid_model
//
//  Created by Aram Markosyan on 3/19/14.
//  Copyright (c) 2014 Aram Markosyan. All rights reserved.
//


#include "High_order.h"

//void High_order::Compute_LF_flux()
//{
//    F_LF.setToValue(0.0);   
//    
//    // left boundary condition: transparent
//    for (int j  = 0; j < 5; j++) {
//        F_LF(j,0) = 0.0;
//    }
//    
//    // right boundary condition: transparent
//    for (int j = 0; j < 5; j++) {
//        F_LF(j,grid) = 0.0;
//    }
//    
//    // elsewhere Lax-Friedrichs flux
//    for (int i = 1; i < grid; i++) {
//        F_LF(0,i) = 0.5 * (N(0,i-1)*N(2,i-1) + N(0,i)*N(2,i)) + 0.5 * dx / dt * (N(0,i-1) - N(0,i));
//        F_LF(1,i) = 0.0;
//        F_LF(2,i) = 0.5 * 2.0/(3.0 * m) * (N(0,i-1)*N(3,i-1) + N(0,i)*N(3,i)) + 0.5 * dx / dt * (N(2,i-1) - N(2,i));
//        F_LF(3,i) = 0.5 * (N(0,i-1)*N(4,i-1) + N(0,i)*N(4,i)) + 0.5 * dx / dt * (N(3,i-1) - N(3,i));
//        F_LF(4,i) = 0.5 * beta * 2.0/(3.0 * m) * (N(0,i-1)*pow(N(3,i-1), 2.0) + N(0,i)*pow(N(3,i), 2.0)) + 0.5 * dx / dt * (N(4,i-1) - N(4,i));
//    }
//}

void High_order::Compute_LF_flux()
{
    F_LF.setToValue(0.0);
    
    // left boundary condition: transparent
    F_LF(0,0) = U(2,0);
    F_LF(1,0) = 0.0;
    F_LF(2,0) = 2.0/(3.0 * m) * U(3,0);
    F_LF(3,0) = U(4,0);
    F_LF(4,0) = beta * 2.0/(3.0 * m) * N(0,0)*pow(N(3,0), 2.0);
    
    // right boundary condition: transparent
    F_LF(0,grid) = U(2,grid - 1);
    F_LF(1,grid) = 0.0;
    F_LF(2,grid) = 2.0/(3.0 * m) * U(3,grid - 1);
    F_LF(3,grid) = U(4,grid - 1);
    F_LF(4,grid) = beta * 2.0/(3.0 * m) * N(0,grid - 1)*pow(N(3,grid - 1), 2.0);
    
    // elsewhere Lax-Friedrichs flux
    for (int i = 1; i < grid; i++) {
        F_LF(0,i) = 0.5 * (U(2,i-1) + U(2,i)) + 0.5 * dx / dt * (U(0,i-1) - U(0,i));
        F_LF(1,i) = 0.0;
        F_LF(2,i) = 0.5 * 2.0/(3.0 * m) * (U(3,i-1) + U(3,i)) + 0.5 * dx / dt * (U(2,i-1) - U(2,i));
        F_LF(3,i) = 0.5 * (U(4,i-1) + U(4,i)) + 0.5 * dx / dt * (U(3,i-1) - U(3,i));
        F_LF(4,i) = 0.5 * beta * 2.0/(3.0 * m) * (N(0,i-1)*pow(N(3,i-1), 2.0) + N(0,i)*pow(N(3,i), 2.0)) + 0.5 * dx / dt * (U(4,i-1) - U(4,i));
    }
}


//void High_order::Compute_RI_flux()
//{
//    F_RI.setToValue(0.0);
//    U_RI.setToValue(0.0);
//    
//    // left boundary condition: transparent
//    for (int j  = 0; j < 5; j++) {
//        U_RI(j,0) = N(j,0);
//    }
//    
//    // right boundary condition: transparent
//    for (int j = 0; j < 5; j++) {
//        U_RI(j,grid) = N(j,grid-1);
//    }
//    
//    // elsewhere intermediate state
//    for (int i = 1; i < grid; i++) {
//        U_RI(0,i) = 0.5 * (N(0,i-1) + N(0,i)) + 0.5 * dt / dx * (N(0,i-1)*N(2,i-1) - N(0,i)*N(2,i));
//        U_RI(1,i) = 0.0;
//        U_RI(2,i) = 0.5 * (N(2,i-1) + N(2,i)) + 0.5 * dt / dx * 2.0/(3.0 * m) * (N(0,i-1)*N(3,i-1) - N(0,i)*N(3,i));
//        U_RI(3,i) = 0.5 * (N(3,i-1) + N(3,i)) + 0.5 * dt / dx * (N(0,i-1)*N(4,i-1) - N(0,i)*N(4,i));
//        U_RI(4,i) = 0.5 * (N(4,i-1) + N(4,i)) + 0.5 * dt / dx * beta * 2.0/(3.0 * m) * (N(0,i-1)*pow(N(3,i-1),2.0) - N(0,i)*pow(N(3,i),2.0));
//    }
//    
//    // Richtmyer flux
//    for (int i = 0; i < grid + 1; i++) {
//        F_RI(0,i) = U_RI(0,i) * U_RI(2,i);
//        F_RI(1,i) = 0.0;
//        F_RI(2,i) = U_RI(0,i) * U_RI(3,i);
//        F_RI(3,i) = U_RI(0,i) * U_RI(4,i);
//        F_RI(4,i) = 2.0/(3.0 * m) * beta * U_RI(0,i) * pow(U_RI(3,i),2.0);
//    }
//    
//}

void High_order::Compute_RI_flux()
{
    F_RI.setToValue(0.0);
    U_RI.setToValue(0.0);
    
    // left boundary condition: transparent
    for (int j  = 0; j < 5; j++) {
        U_RI(j,0) = U(j,0);
    }
    
    // right boundary condition: transparent
    for (int j = 0; j < 5; j++) {
        U_RI(j,grid) = U(j,grid-1);
    }
    
    // elsewhere intermediate state
    for (int i = 1; i < grid; i++) {
        U_RI(0,i) = 0.5 * (U(0,i-1) + U(0,i)) + 0.5 * dt / dx * (U(2,i-1) - U(2,i));
        U_RI(1,i) = 0.0;
        U_RI(2,i) = 0.5 * (U(2,i-1) + U(2,i)) + 0.5 * dt / dx * 2.0/(3.0 * m) * (U(3,i-1) - U(3,i));
        U_RI(3,i) = 0.5 * (U(3,i-1) + U(3,i)) + 0.5 * dt / dx * (U(4,i-1) - U(4,i));
        U_RI(4,i) = 0.5 * (U(4,i-1) + U(4,i)) + 0.5 * dt / dx * beta * 2.0/(3.0 * m) * (N(0,i-1)*pow(N(3,i-1),2.0) - N(0,i)*pow(N(3,i),2.0));
    }
    
    eps_for_RI.setToValue(0.0);
    UtoEps(U_RI, eps_for_RI);
    
    // Richtmyer flux
    for (int i = 0; i < grid + 1; i++) {
        F_RI(0,i) = U_RI(2,i);
        F_RI(1,i) = 0.0;
        F_RI(2,i) = 2.0/(3.0 * m) * U_RI(3,i);
        F_RI(3,i) = U_RI(4,i);
        F_RI(4,i) = 2.0/(3.0 * m) * beta * U_RI(0,i) * pow(eps_for_RI(i),2.0);
    }
    
}

void High_order::FORCE()
{
    U_new.setToValue(0.0);
    for (int i = 0; i < grid; i++) {
        U_new(0,i) = U(0,i) + dt/dx * (F_FORCE(0,i) - F_FORCE(0,i+1)) + dt * U(0,i) * nuI(abs(N(3,i)));
        U_new(1,i) = U(1,i) + dt * U(0,i) * nuI(abs(N(3,i)));
        U_new(2,i) = U(2,i) + dt/dx * (F_FORCE(2,i) - F_FORCE(2,i+1)) + dt * (electron/m * U(0,i)*0.5*(E(i) + E(i-1)) - nuM(abs(N(3,i)))* U(2,i));
        U_new(3,i) = U(3,i) + dt/dx * (F_FORCE(3,i) - F_FORCE(3,i+1)) + dt * (electron*0.5*(E(i) + E(i-1))*U(2,i) - nuE(abs(N(3,i)))*U(3,i) - U(0,i)*(-1.5*k*nuE(abs(N(3,i)))*T_0 + epsI*nuI(abs(N(3,i))) + nuTot(abs(N(3,i))) ));
        U_new(4,i) = U(4,i) + dt/dx * (F_FORCE(4,i) - F_FORCE(4,i+1)) + dt * (5.0/3.0 * electron/m * 0.5*(E(i) + E(i-1)) * U(3,i) - nuM(abs(N(3,i)))*U(4,i));
    }
    U = U_new;
    //U.save_to_file(Folder, "U.txt");
    //U_new.save_to_file(Folder, "U_new.txt");
    //cout<<endl;
}

void High_order::Compute_FORCE_flux()
{
    F_FORCE.setToValue(0.0);
    
    for (int i = 0; i < grid + 1; i++){
        for (int j = 0; j < 5; j++) {
            F_FORCE(j,i) = 0.5 * (F_LF(j,i) + F_RI(j,i));
        }
    }
}


// Run baby in controled way ;)
void High_order::Propagate_FORCE()
{
    while (t < final_time) {
        Dt(N);
        for (int i = 0; i<grid; i++) {
            if (E(i) < 0) {
                cout << "Negative density encountered :(" << endl;
                grid = i - 10;
                //exit(1);
            }
            if (E(i) > field_vm){
                E(i) = field_vm;
            }
        }
        
        Compute_LF_flux();
        Compute_RI_flux();
        Compute_FORCE_flux();
        FORCE();
        UtoN(U, N);
        Update_field(U);
        
        t = t + dt;
        general_step++;
        
//        if (general_step%200 == 0) {
//            cout << "Output number " << general_step << " written with dt "<<dt<<"\n";
//            Output();
//        }
        
    }
}

