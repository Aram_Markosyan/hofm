//
//  High_order_basic.cpp
//  High_order_fluid_model
//
//  Created by Aram Markosyan on 3/19/14.
//  Copyright (c) 2014 Aram Markosyan. All rights reserved.
//


#include "High_order.h"

// Return interplolated value
double High_order::interp(double x,
                          vector<pair<double,double> >& table)
{
    const double INF = 1.e100;
    // Assumes that "table" is sorted by .first
    // If you are not sure if table is sorted:
    // sort(table.begin(), table.end());
    
    // Check if x is out of bound
    if (x > table.back().first) return table.back().second;
    if (x < table[0].first) return table[0].second;
    vector<pair<double, double> >::iterator it, it2;
    // INFINITY is defined in math.h in the glibc implementation
    it = lower_bound(table.begin(), table.end(), make_pair(x, -INF));
    // Corner case
    if (it == table.begin()) return it->second;
    it2 = it;
    --it2;
    return it2->second + (it->second - it2->second)*(x - it2->first)/(it->first - it2->first);
}

// Constructor
High_order::High_order(const string& folder):
x(1),U(1,1),N(1,1),E(1)
{
    if (folder == "") {
        ifstream input_file("Input/input.txt");
        if (!input_file) {
            input_file.close();
            input_file.open("/Users/aram/Desktop/Comparison_final coments/High_order_FORCE/High_order_fluid_model/Input/input.txt");
            if (input_file) {
                Folder = "/Users/aram/Desktop/Comparison_final coments/High_order_FORCE/High_order_fluid_model/Input/";
                input_file.close();
            }else {
                /* could not open directory */
                perror ("Input folder or input file");
                exit(EXIT_FAILURE);
            }
        } else {
            input_file.close();
            Folder = "Input/";
        }
    } else {
        if (folder.at(folder.size()-1) == '/') Folder = folder;
        else Folder = folder + "/";
    }
    
    charge = 1.60022e-19;
    permetivity = 8.85418782e-12;
}

// Read input files
void High_order::Load_input()
{
    Read_param();
    Read_input();
    Init_problem();
}

// Read input parameters
void High_order::Read_param()
{
    string filename = Folder+"input.txt";
    ifstream file(filename.c_str());
    
    if (!file) {
        // file couldn't be opened
        cerr << "Error: File containing the parameters could not be opened" << endl;
        exit(1);
    }
    
    string line;
    for (int i = 0; i < 10; i++) {
        getline(file,line);
        string temp1;
        istringstream iss(line);
        iss>>temp1;
        iss>>temp1;
        if (i == 0) iss>>gas;
        if (i == 1) iss>>field_td;
        if (i == 2) iss>>grid;
        if (i == 3) iss>>final_x;
        if (i == 4) iss>>final_time;
        if (i == 5) iss>>CFL;
        if (i == 6) iss>>const_time_step;
        if (i == 7) iss>>time_step_points;
        if (i == 8) iss>>beta;
        if (i == 9) iss>>attachement;
        
    }
    file.close();
}

// Read transport coefficients
void High_order::Read_input()
{
    string filename = Folder+gas+"_input.txt";
    ifstream file(filename.c_str());
    if (!file) {
        // file couldn't be opened
        cerr << "Error: File containing the transport coefficients for "<<gas<<" could not be opened" << endl;
        exit(1);
    }
    
    string line;
    getline(file, line);
    getline(file, line);
    
    while(getline(file,line)) {
        istringstream iss(line);
        double val1, val2, val3, val4, val5, val6, val7, val8, val9, val10;
        iss>>val1; iss>>val2; iss>>val3;
        iss>>val4; iss>>val5; iss>>val6;
        iss>>val7; iss>>val8; iss>>val9;
        if (attachement) iss>>val10;
        
        m_E_td.push_back(make_pair(val1, val2));
        m_v_E.push_back(make_pair(val2, val5));
        m_eps_E.push_back(make_pair(val2, val4));
        m_nuM.push_back(make_pair(val4, val6));
        m_nuE.push_back(make_pair(val4, val7));
        m_nuTot.push_back(make_pair(val4, val8));
        m_nuI.push_back(make_pair(val4, val9));
        if (attachement) m_nuA.push_back(make_pair(val4, val10));
    }
    file.close();
    
    gas_epsI.push_back(2.52503027e-18);   // Ar
    gas_epsI.push_back(3.94135435e-18);   // He
    gas_epsI.push_back(3.45429267e-18);   // Ne
    gas_epsI.push_back(1.94344017e-18);   // Xe
    gas_epsI.push_back(2.49939544e-18);   // N2
}

// Make a grid and density initialization
void High_order::Init_problem()
{
    if ( (gas == "Ar_flux") || (gas == "Ar_bulk") )
        epsI = gas_epsI[0];
    else if ( (gas == "He_flux") || (gas == "He_bulk") )
        epsI = gas_epsI[1];
    else if ( (gas == "Ne_flux") || (gas == "Ne_bulk") )
        epsI = gas_epsI[2];
    else if ( (gas == "Xe_flux") || (gas == "Xe_bulk") )
        epsI = gas_epsI[3];
    else if ( (gas == "N2_flux") || (gas == "N2_bulk") )
        epsI = gas_epsI[4];
    else
        cerr << "Error: Specify correct gas mixture!" << endl;
    
    // Field in V/m
    field_vm = interp(field_td, m_E_td);
    double initial_velocity = interp(field_vm, m_v_E);
    double initial_energy = interp(field_vm, m_eps_E);
    
    general_step = 0;
    t = 0;
    m = 9.10938e-31;
    k = 1.38065e-23;
    T_0 = 298;
    electron = -1.6022e-19;
    
    x.initialize(grid);
    U.initialize(5, grid);
    N.initialize(5, grid);
    E.initialize(grid+1);
    
    U_new.initialize(5,grid);
    
    F_LF.initialize(5, grid + 1);
    F_RI.initialize(5, grid + 1);
    U_RI.initialize(5, grid + 1);
    F_FORCE.initialize(5, grid + 1);
    eps_for_RI.initialize(grid + 1);
    
    
    min_density = 1.0e-10;
    
    dx = final_x/grid;
    for (int i = 0; i < grid; i++) {
        x(i) = dx/2.0 + i*dx;
        
        N(0,i) = 2.0e18*exp(-(x(i)- 9.0e-3)*(x(i)- 9.0e-3)*2000.*760*760);
        
        N(1,i) = N(0,i);
        N(2,i) = - initial_velocity;
        N(3,i) = initial_energy;
        N(4,i) = -5./3. * initial_energy*initial_velocity;
    }
    Update_field(N);
    NtoU(N, U);
    UtoN(U, N);
    Output();
}

High_order::~High_order(){
    
}

void High_order::NtoU(const doublearray2d& N_dens,
                      doublearray2d&       U_dens)
{
    for (int i = 0; i < grid; i++) {
        U_dens(0,i) = N_dens(0,i);
        U_dens(1,i) = N_dens(1,i);
        U_dens(2,i) = N_dens(0,i) * N_dens(2,i);
        U_dens(3,i) = N_dens(0,i) * N_dens(3,i);
        U_dens(4,i) = N_dens(0,i) * N_dens(4,i);
    }
}

void High_order::UtoN(const doublearray2d& U_dens,
                      doublearray2d&       N_dens)
{
    double local_E;
    double field_v;
    double field_eps;
    
    for (int i = 0; i < grid; i++) {
        local_E = 0.5*(E(i) + E(i+1));
        
        
        field_eps = interp(abs(local_E), m_eps_E);
        N_dens(0,i) = U_dens(0,i);
        N_dens(1,i) = U_dens(1,i);
        
        if (local_E > 0) {
            field_v = -interp(abs(local_E), m_v_E);
        } else {
            field_v = interp(abs(local_E), m_v_E);
        }
        
        N_dens(2,i) = (U_dens(2,i) + min_density * field_v)
        / (U_dens(0,i) + min_density);
        N_dens(3,i) = (U_dens(3,i) + min_density * field_eps)
        / (U_dens(0,i) + min_density);
        // Jannis: added 5/3 here
        N_dens(4,i) = (U_dens(4,i) + 5./3. * min_density * field_eps * field_v)
        / (U_dens(0,i) + min_density);
        // cout << i << ": " << local_E << ", " << field_v << ", " << field_eps << ", " << N_dens(0,i) << ", " << N_dens(2,i) << endl;
    }
}

// Solves the Poisson equation
void High_order::Update_field(const doublearray2d& n_dens)
{
    E(0) = field_vm;
    for (int i = 1; i < grid + 1; i ++) {
        E(i) = E(i-1) + dx * charge  * (n_dens(1,i-1) - n_dens(0,i-1))/permetivity;
        // E(i) = max(E(i),0.0);
    }
}


// Get minimal dt
void High_order::Dt(doublearray2d& u)
{
    if (const_time_step == 1) dt = final_time / time_step_points;
    else {
        if (beta == 0) cerr << "\beta = 0. Time step can not be calculated." << endl;
        double max_energy = -1.0e18;
        for (int i = 0; i < grid; i++)
            if (max_energy < N(3,i)) max_energy = N(3,i);
        dt = CFL * 2.0 * dx / (beta * sqrt(2.0* max_energy/3.0/m));
    }
    
    // cout<<"Calculated dt: "<<dt<<". General time: "<<t<<" x "<<general_step<<endl;
}

void High_order::Print()
{
    cout<<"Gas = "<<gas<<endl;
    cout<<"field = "<<field_vm<<endl;
    cout<<"grid = "<<grid<<endl;
    cout<<"final_x = "<<final_x<<endl;
    cout<<"final_time = "<<final_time<<endl;
    cout<<"CFL = "<<CFL<<endl;
    cout<<"const_time_step = "<<const_time_step<<endl;
    cout<<"time_step = "<<time_step_points<<endl;
    cout<<"beta = "<<beta<<endl;
    
    ofstream file;
    string filename;
    stringstream tempik;
    tempik<<general_step;
    filename = Folder+"/Output/"+gas+"_head_pos.txt";
    file.open(filename.c_str());
    //file.precision(3);
    file.setf(ios::scientific | ios::showpoint);
    cout.precision(6);
    //cout.setf(ios::fixed | ios::showpoint);
    for (int i = 0; i < head_pos.size(); i++)
        file<<setw(16)<<head_time[i]<<setw(16)<<head_pos[i]<<endl;
}


// Outputting all
void High_order::Output()
{
    ofstream file;
    string filename;
    stringstream tempik;
    tempik<<general_step;
    filename = Folder+"/Output/"+gas+"_step_"+tempik.str()+".txt";
    file.open(filename.c_str());
    //file.precision(3);
    file.setf(ios::scientific | ios::showpoint);
    cout.precision(6);
    //cout.setf(ios::fixed | ios::showpoint);
    for (int i = 1; i < grid; i++)
        file<<setw(16)<<x(i)<<setw(16)<<N(0,i)<<setw(16)<<N(1,i)
        <<setw(16)<<0.5*(E(i)+E(i+1))<<setw(16)<<N(2,i)<<setw(16)<<N(3,i)/1.60217646e-19
        <<setw(16)<<N(4,i)/1.60217646e-19<<setw(16)<<t<<endl;
    
    
    int pos = -5;
    head_time.push_back(t);
    for (int i = 1; i < grid; i++) {
        if (N(0,i) > 2.0e18) {
            pos = i;
            break;
        }
    }
    
    double x_position = x(pos - 1) + (2.0e18 - N(0,pos-1)) * (x(pos) - x(pos-1))/(N(0,pos)-N(0,pos-1));
    
    head_pos.push_back(x_position);
    
}


// Returns nuM
double High_order::nuM(const double x){
    return interp(abs(x), m_nuM);
}

// Returns nuE
double High_order::nuE(const double x){
    return interp(abs(x), m_nuE);
}

// Returns nuTot
double High_order::nuTot(const double x){
    return interp(abs(x), m_nuTot);
}

// Returns nuI
double High_order::nuI(const double x){
    return interp(abs(x), m_nuI);
}

// Returns nuA
double High_order::nuA(const double x){
    return interp(abs(x), m_nuA);
}

void High_order::UtoEps(const doublearray2d& U_dens,
                        doublearray1d&       eps)
{
    double local_E;
    double field_eps;
    for (int i = 0; i < grid; i++) {
        
        if (i == 0) local_E = 0.5 * (E(0) + E(1));
        else if (i == grid - 1) local_E = 0.5 * (E(grid-1) + E(grid));
        else local_E = 0.5 * (E(i) + E(i-1));
        
        field_eps = interp(abs(local_E), m_eps_E);
        
        eps(i) = (U_dens(3,i) + min_density * field_eps)/ (U_dens(0,i) + min_density);
        // eps(i) = field_eps;
    }
}

