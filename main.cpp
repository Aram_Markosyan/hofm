//
//  main.cpp
//  High_order_fluid_model
//
//  Created by Aram Markosyan on 6/15/13.
//  Copyright (c) 2013 Aram Markosyan. All rights reserved.
//

#include <iostream>
#include <utility>
#include <vector>
#include "High_order.h"

using namespace std;

int main(int argc, const char * argv[])
{
    string folder = "";
    if (argc > 1) folder = string(argv[1]);
    
    High_order streamer(folder);
    streamer.Load_input();
    //streamer.Print();
    
    streamer.Propagate_FORCE_2nd();
    //streamer.Propagate_FORCE();
    //streamer.Propagate();
    
    streamer.Output();
    streamer.Print();
    
    cout<<"\n"<<"Everything is Cool!"<<endl;
    return 0;
}

