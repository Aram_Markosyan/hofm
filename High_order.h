//
//  High_order.h
//  High_order_fluid_model
//
//  Created by Aram Markosyan on 6/15/13.
//  Copyright (c) 2013 Aram Markosyan. All rights reserved.
//

#ifndef __High_order_fluid_model__High_order__
#define __High_order_fluid_model__High_order__

#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iomanip>
#include "stdlib.h"
#include "Array.h"

using namespace std;

class High_order {
    vector<pair<double,double> > m_E_td;    // V/m as a function of Td
    vector<pair<double,double> > m_v_E;     // velocity as a function of V/m
    vector<pair<double,double> > m_eps_E;   // J as a function of V/m
    vector<pair<double,double> > m_nuM;     // nuM as a function of J
    vector<pair<double,double> > m_nuE;     // nuE as a function of J
    vector<pair<double,double> > m_nuTot;   // nuTot as a function of J
    vector<pair<double,double> > m_nuI;     // nuI as a function of J
    vector<pair<double,double> > m_nuA;     // nuA as a function of J
    
    vector<double> gas_epsI;    // ionization threshold
    double epsI;
    
    doublearray1d x;
    doublearray2d U;    // n, n_ion, n*v, n*eps, n*qsi
    doublearray2d N;    // n, n_ion, v, eps, qsi
    doublearray1d E;
    
    doublearray2d U_new;
    
    doublearray2d F_LF;
    doublearray2d F_RI;
    doublearray2d U_RI;
    doublearray2d F_FORCE;
    doublearray1d eps_for_RI;
    
    doublearray2d U_L;
    doublearray2d U_R;
    double omega; //  -1 >= omega <= 1
    doublearray2d U_diff;
    doublearray2d U_int_L;
    doublearray2d U_int_R;
    doublearray1d eps_for_U_L;
    doublearray1d eps_for_U_R;
    doublearray2d F_LF_2nd;
    doublearray2d U_RI_int;
    doublearray1d eps_for_RI_2nd;
    doublearray2d F_RI_2nd;
    doublearray2d F_FORCE_2nd;
    
    
    double charge;
    double permetivity;
    
    string Folder;
    
    // field in Td and vm
    double field_td;
    double field_vm;
    
    // Minimal density
    double min_density;
    
    // Gas name
    string gas;
    bool attachement;
    
    int grid;
    double final_x;
    double dx;
    double dt;
    
    double CFL;
    double final_time;
    bool const_time_step;
    int time_step_points;
    double beta;
    double m;
    double k;
    double T_0;
    double electron;
    
    int general_step;
    double t;
    
    // Read input parameters
    void Read_param();
    
    // Read transport coefficients
    void Read_input();
    
    // Return interplolated value
    double interp(double x,
                  vector<pair<double,double> >& table);
    
    // Make a grid and density initialization
    void Init_problem();
    
    // Solves the Poisson equation
    void Update_field(const doublearray2d& n_dens);
    
    void NtoU(const doublearray2d& N_dens,
              doublearray2d&       U_dens);
    
    void UtoN(const doublearray2d& U_dens,
              doublearray2d&       N_dens);
    
    void UtoEps(const doublearray2d& U_dens,
                doublearray1d&       eps);
    
    void Flux(doublearray2d& flux);
    
    void Compute_LF_flux();
    void Compute_RI_flux();
    void Compute_FORCE_flux();
    void FORCE();
    
    void Boundary_extrapolated_values();
    void Compute_diff();
    void Compute_intermidiate();
    void Compute_force();
    void Compute_LF_for_2nd_order();
    void Compute_RI_intermidiate();
    void Compute_RI_for_2nd_order();
    void Compute_FORCE_flux_for_2nd_order();
    void FORCE_2();
    
    
    // Returns nuM
    double nuM(const double x);
    // Returns nuE
    double nuE(const double x);
    // Returns nuTot
    double nuTot(const double x);
    // Returns nuI
    double nuI(const double x);
    // Returns nuA
    double nuA(const double x);
    
    // Get minimal dt
    void Dt(doublearray2d& u);
    
    void RK4(const doublearray2d& u,
             doublearray2d&       K);
    
    vector<double> head_time;
    vector<double> head_pos;
    
    
public:
    
    // Constructor
    High_order(const string& folder);
    ~High_order();
    
    // Read input files
    void Load_input();
    
    // Outputting all
    void Output();
    
    // Run baby, run!
    void Propagate();
    
    // Run baby in controled way ;)
    void Propagate_FORCE();
    
    void Propagate_FORCE_2nd();
    
    void Print();
    
    
};


#endif /* defined(__High_order_fluid_model__High_order__) */
