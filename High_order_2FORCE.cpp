//
//  High_order_2FORCE.cpp
//  High_order_fluid_model
//
//  Created by Aram Markosyan on 3/19/14.
//  Copyright (c) 2014 Aram Markosyan. All rights reserved.
//


#include "High_order.h"


void High_order::Boundary_extrapolated_values()
{
    U_L.setToValue(0.0);
    U_R.setToValue(0.0);
    
    for (int i = 0; i < grid; i++) {
        for (int j = 0; j < 5; j++) {
            U_L(j,i) = U(j,i) - 0.5 * U_diff(j,i);
            U_R(j,i) = U(j,i) + 0.5 * U_diff(j,i);
        }
    }
}

void High_order::Compute_intermidiate()
{
    U_int_L.setToValue(0.0);
    U_int_R.setToValue(0.0);
    
    eps_for_U_L.setToValue(0.0);
    eps_for_U_R.setToValue(0.0);
    
    UtoEps(U_L, eps_for_U_L);
    UtoEps(U_R, eps_for_U_R);
    
    
    for (int i = 0; i < grid; i++) {
        U_int_L(0,i) = U_L(0,i) + 0.5 * dt/dx * (U_L(2,i) - U_R(2,i));
        U_int_L(1,i) = 0.0;
        U_int_L(2,i) = U_L(2,i) + 0.5 * dt/dx * 2.0 / (3.0 * m) * (U_L(3,i) - U_R(3,i));
        U_int_L(3,i) = U_L(3,i) + 0.5 * dt/dx * (U_L(4,i) - U_R(4,i));
        U_int_L(4,i) = U_L(4,i) + 0.5 * dt/dx * beta * 2.0 / (3.0 * m) * (U_L(0,i)*pow(eps_for_U_L(i), 2.0) - U_R(0,i)*pow(eps_for_U_R(i), 2.0));
    }
    
    for (int i = 0; i < grid; i++) {
        U_int_R(0,i) = U_R(0,i) + 0.5 * dt/dx * (U_L(2,i) - U_R(2,i));
        U_int_R(1,i) = 0.0;
        U_int_R(2,i) = U_R(2,i) + 0.5 * dt/dx * 2.0 / (3.0 * m) * (U_L(3,i) - U_R(3,i));
        U_int_R(3,i) = U_R(3,i) + 0.5 * dt/dx * (U_L(4,i) - U_R(4,i));
        U_int_R(4,i) = U_R(4,i) + 0.5 * dt/dx * beta * 2.0 / (3.0 * m) * (U_L(0,i)*pow(eps_for_U_L(i), 2.0) - U_R(0,i)*pow(eps_for_U_R(i), 2.0));
    }
}

void High_order::Compute_LF_for_2nd_order()
{
    F_LF_2nd.setToValue(0.0);
    
    eps_for_U_L.setToValue(0.0);
    eps_for_U_R.setToValue(0.0);
    
    UtoEps(U_int_L, eps_for_U_L);
    UtoEps(U_int_R, eps_for_U_R);
    
    // left boundary conditions
    F_LF_2nd(0,0) = 0.5 * (U(2,0) + U_int_L(2,0)) + 0.5 * dx/dt * (U(0,0) - U_int_L(0,0));
    F_LF_2nd(1,0) = 0.0;
    F_LF_2nd(2,0) = 0.5 * 2.0/(3.0 * m) * (U(3,0) + U_int_L(3,0)) + 0.5 * dx/dt * (U(2,0) - U_int_L(2,0));
    F_LF_2nd(3,0) = 0.5 * (U(4,0) + U_int_L(4,0)) + 0.5 * dx/dt * (U(3,0) - U_int_L(3,0));
    F_LF_2nd(4,0) = 0.5 * 2.0/(3.0 * m) * beta * (U(0,0)*pow(N(3,0),2.0) + U_int_L(0,0)*pow(eps_for_U_L(0),2.0)) + 0.5 * dx/dt * (U(4,0) - U_int_L(4,0));
    
    // right boundary conditions
    F_LF_2nd(0,grid) = 0.5 * (U_int_R(2,grid-1) + U(2,grid-1)) + 0.5 * dx/dt * (U_int_R(0,grid-1) - U(0,grid-1));
    F_LF_2nd(1,grid) = 0.0;
    F_LF_2nd(2,grid) = 0.5 * 2.0/(3.0 * m) * (U_int_R(3,grid-1) + U(3,grid-1)) + 0.5 * dx/dt * (U_int_R(2,grid-1) - U(2,grid-1));
    F_LF_2nd(3,grid) = 0.5 * (U_int_R(4,grid-1) + U(4,grid-1)) + 0.5 * dx/dt * (U_int_R(3,grid-1) - U(3,grid-1));
    F_LF_2nd(4,grid-1) = 0.5 * 2.0/(3.0 * m) * beta * (U_int_R(0,grid-1)*pow(eps_for_U_R(grid-1),2.0) + U(0,grid-1)*pow(N(3,grid-1),2.0)) + 0.5 * dx/dt * (U_int_R(4,grid-1) - U(4,grid-1));
    
     // elsewhere Lax-Friedrichs flux
    for (int i = 1; i < grid; i++) {
        F_LF_2nd(0,i) = 0.5 * (U_int_R(2,i-1) + U_int_L(2,i)) + 0.5 * dx/dt * (U_int_R(0,i-1) - U_int_L(0,i));
        F_LF_2nd(1,i) = 0.0;
        F_LF_2nd(2,i) = 0.5 * 2.0/(3.0 * m) * (U_int_R(3,i-1) + U_int_L(3,i)) + 0.5 * dx/dt * (U_int_R(2,i-1) - U_int_L(2,i));
        F_LF_2nd(3,i) = 0.5 * (U_int_R(4,i-1) + U_int_L(4,i)) + 0.5 * dx/dt * (U_int_R(3,i-1) - U_int_L(3,i));
        F_LF_2nd(4,i) = 0.5 * 2.0/(3.0 * m) * beta * (U_int_R(0,i-1)*pow(eps_for_U_R(i-1), 2.0) + U_int_L(0,i)*pow(eps_for_U_L(i), 2.0)) + 0.5 * dx/dt * (U_int_R(4,i-1) - U_int_L(4,i));
    }
}

void High_order::Compute_RI_intermidiate()
{
    // left boundary conditions
    U_RI_int(0,0) = 0.5 * (U(0,0) + U_int_L(0,0)) + 0.5 * dt/dx * (U(2,0) - U_int_L(2,0));
    U_RI_int(1,0) = 0.0;
    U_RI_int(2,0) = 0.5 * (U(2,0) + U_int_L(2,0)) + 0.5 * dt/dx * 2.0/(3.0 * m) * (U(3,0) - U_int_L(3,0));
    U_RI_int(3,0) = 0.5 * (U(3,0) + U_int_L(3,0)) + 0.5 * dt/dx * (U(4,0) - U_int_L(4,0));
    U_RI_int(4,0) = 0.5 * (U(4,0) + U_int_L(4,0)) + 0.5 * dt/dx * 2.0/(3.0 * m) * beta * (U(0,0)*pow(N(3,0),2.0) - U_int_L(0,0)*pow(eps_for_U_L(0),2.0));
    
    
    // right boundary conditions
    U_RI_int(0,grid) = 0.5 * (U_int_R(0,grid-1) + U(0,grid-1)) + 0.5 * dt/dx * (U_int_R(2,grid-1) - U(2,grid-1));
    U_RI_int(1,grid) = 0.0;
    U_RI_int(2,grid) = 0.5 * (U_int_R(2,grid-1) + U(2,grid-1)) + 0.5 * dt/dx * 2.0/(3.0 * m) * (U_int_R(3,grid-1) - U(3,grid-1));
    U_RI_int(3,grid) = 0.5 * (U_int_R(3,grid-1) + U(3,grid-1)) + 0.5 * dt/dx * (U_int_R(4,grid-1) - U(4,grid-1));
    U_RI_int(4,grid) = 0.5 * (U_int_R(4,grid-1) + U(4,grid-1)) + 0.5 * dt/dx * 2.0/(3.0 * m) * beta * (U_int_R(0,grid-1)*pow(eps_for_U_R(grid-1),2.0) - U(0,grid-1)*pow(N(3,grid-1),2.0));
    
    
    // elsewhere
    for (int i = 1; i < grid; i++) {
        U_RI_int(0,i) = 0.5 * (U_int_R(0,i-1) + U_int_L(0,i)) + 0.5 * dt/dx * (U_int_R(2,i-1) - U_int_L(2,i));
        U_RI_int(1,i) = 0.0;
        U_RI_int(2,i) = 0.5 * (U_int_R(2,i-1) + U_int_L(2,i)) + 0.5 * 2.0/(3.0 * m) * dt/dx * (U_int_R(3,i-1) - U_int_L(3,i));
        U_RI_int(3,i) = 0.5 * (U_int_R(3,i-1) + U_int_L(3,i)) + 0.5 * dt/dx * (U_int_R(4,i-1) - U_int_L(4,i));
        U_RI_int(4,i) = 0.5 * (U_int_R(4,i-1) + U_int_L(4,i)) + 0.5 * dt/dx * 2.0/(3.0 * m) * beta * (U_int_R(0,i-1)*pow(eps_for_U_R(i-1), 2.0) - U_int_L(0,i)*pow(eps_for_U_L(i), 2.0));
    }
}

void High_order::Compute_RI_for_2nd_order()
{
     UtoEps(U_RI_int, eps_for_RI_2nd);
    
    // Richtmyer flux
    for (int i = 0; i < grid + 1; i++) {
        F_RI_2nd(0,i) = U_RI_int(2,i);
        F_RI_2nd(1,i) = 0.0;
        F_RI_2nd(2,i) = 2.0/(3.0 * m) * U_RI_int(3,i);
        F_RI_2nd(3,i) = U_RI_int(4,i);
        if (i < grid)
            F_RI_2nd(4,i) = 2.0/(3.0 * m) * beta * U_RI_int(0,i) * pow(eps_for_RI_2nd(i),2.0);
        else
            F_RI_2nd(4,i) = 2.0/(3.0 * m) * beta * U_RI_int(0,i) * pow(eps_for_RI_2nd(i-1),2.0);
    }
}

void High_order::Propagate_FORCE_2nd()
{
    while (t < final_time) {
        Dt(N);
        for (int i = 0; i<grid; i++) {
            //if (E(i) < 0) {
            if (N(3,i) < 0) {

                cout << "Negative density encountered :(" << endl;
                grid = i - 10;
                //exit(1);
            }
            if (E(i) > field_vm){
                E(i) = field_vm;
            }
        }
        
        U_L.initialize(5, grid);
        U_R.initialize(5, grid);
        U_diff.initialize(5, grid);
        U_int_L.initialize(5, grid);
        U_int_R.initialize(5, grid);
        
        eps_for_U_L.initialize(grid);
        eps_for_U_R.initialize(grid);
        eps_for_RI_2nd.initialize(grid);
        
        F_LF_2nd.initialize(5, grid + 1);
        U_RI_int.initialize(5, grid + 1);
        F_RI_2nd.initialize(5, grid + 1);
        F_FORCE_2nd.initialize(5, grid + 1);
        
        omega = 0.5;
        
        UtoN(U, N);
        
        Compute_diff();
        Boundary_extrapolated_values();
        Compute_intermidiate();
        Compute_LF_for_2nd_order();
        Compute_RI_intermidiate();
        Compute_RI_for_2nd_order();
        Compute_FORCE_flux_for_2nd_order();
        FORCE_2();
        
        UtoN(U, N);
        Update_field(U);
        
        t = t + dt;
        general_step++;
        
        if (general_step%500 == 0) {
            cout << "Output number " << general_step << " written with dt "<<dt<<"\n";
            Output();
        }
        
    }
    
}

void High_order::Compute_FORCE_flux_for_2nd_order()
{
    for (int i = 0; i < grid + 1; i++){
        for (int j = 0; j < 5; j++) {
            F_FORCE_2nd(j,i) = 0.5 * (F_LF_2nd(j,i) + F_RI_2nd(j,i));
        }
    }
}

void High_order::FORCE_2()
{
    U_new.setToValue(0.0);
    for (int i = 0; i < grid; i++) {
        U_new(0,i) = U(0,i) + dt/dx * (F_FORCE_2nd(0,i) - F_FORCE_2nd(0,i+1)) + dt * U(0,i) * nuI(abs(N(3,i)));
        U_new(1,i) = U(1,i) + dt * U(0,i) * nuI(abs(N(3,i)));
        U_new(2,i) = U(2,i) + dt/dx * (F_FORCE_2nd(2,i) - F_FORCE_2nd(2,i+1)) + dt * (electron/m * U(0,i)*0.5*(E(i) + E(i-1)) - nuM(abs(N(3,i)))* U(2,i));
        U_new(3,i) = U(3,i) + dt/dx * (F_FORCE_2nd(3,i) - F_FORCE_2nd(3,i+1)) + dt * (electron*0.5*(E(i) + E(i-1))*U(2,i) - nuE(abs(N(3,i)))*U(3,i) - U(0,i)*(-1.5*k*nuE(abs(N(3,i)))*T_0 + epsI*nuI(abs(N(3,i))) + nuTot(abs(N(3,i))) ));
        U_new(4,i) = U(4,i) + dt/dx * (F_FORCE_2nd(4,i) - F_FORCE_2nd(4,i+1)) + dt * (5.0/3.0 * electron/m * 0.5*(E(i) + E(i-1)) * U(3,i) - nuM(abs(N(3,i)))*U(4,i));
    }
    U = U_new;
}


void High_order::Compute_diff()
{
    U_diff.setToValue(0.0);
    
    // left boundary condition transparent
    for (int j = 0; j < 5; j++) {
        U_diff(j,0) = 0.5 * (1 - omega) * (U(j,1) - U(j,0));
    }
    
    // right boundary condition transparent
    for (int j = 0; j < 5; j++) {
        U_diff(j, grid - 1) = 0.5 * (1 + omega) * (U(j,grid - 1) - U(j,grid - 2));
    }
    
    for (int i = 1; i < grid - 1; i++) {
        for (int j = 0; j < 5; j++) {
            U_diff(j,i) = 0.5 * (1 + omega) * (U(j,i) - U(j,i-1)) + 0.5 * (1 - omega) * (U(j,i + 1) - U(j,i));
        }
    }
}
















