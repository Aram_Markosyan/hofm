//
//  Array.h
//  First_order_streamer
//
//  Created by Aram Markosyan on 5/18/13.
//  Copyright (c) 2013 Aram Markosyan. All rights reserved.
//

#ifndef __First_order_streamer__Array__
#define __First_order_streamer__Array__

#include<iostream>
#include "stdlib.h"
#include <fstream>
#include <sstream>
#include <iomanip>

using namespace std;

class doublearray1d
{
	friend ostream& operator << (ostream& outstream, const doublearray1d& d);
	
private:
	
	double* dataPtr;
	long    index1Begin;
	long    index1End;
	long    index1Size;
	int     internalAlloc;
	
public:
	
	doublearray1d();
	doublearray1d(long size);
	doublearray1d(const doublearray1d& d);
	~doublearray1d();
	void initialize(long m);
	void initialize(const doublearray1d& d);
	double&  operator()(long i1);
	const double&  operator()(long i1) const;
	double* getDataPointer();
    
    friend doublearray1d operator+(const doublearray1d& A1,
                                   const doublearray1d& A2);
    
    friend doublearray1d operator-(const doublearray1d& A1,
                                   const doublearray1d& A2);
    
    friend double operator*(const doublearray1d& A1,
                            const doublearray1d& A2);
    
    friend doublearray1d operator*(const double&        a1,
                                   const doublearray1d& A2);
    
    friend doublearray1d operator*(const doublearray1d& A1,
                                   const double&        a2);

	
	void setIndex1Begin(long i);
	long getIndex1Begin() const;
	long getIndex1End() const;
	long getIndex1Size() const;
    
	void resize(long newSize);
	void operator=(const doublearray1d& d);
	
	void setToValue(double d);
	void addValue(double d);
};

class doublearray2d
{
	friend ostream& operator << (ostream& outstream, const doublearray2d& d);
	
private:
	
	double* dataPtr;
	long index1Begin;
	long index1End;
	long index1Size;
	long index2Begin;
	long index2End;
	long index2Size;
	int internalAlloc;
	
public:
	
	doublearray2d();
	doublearray2d(long size1, long size2);
	doublearray2d(const doublearray2d& d);
	~doublearray2d();
	void initialize(long size1, long size2);
	void initialize(const doublearray2d& d);
	double&  operator()(long i1, long i2);
	const double& operator()(long i1, long i2) const;
	double* getDataPointer();
    
    friend doublearray2d operator+(const doublearray2d& A1,
                                   const doublearray2d& A2);
    
    friend doublearray2d operator-(const doublearray2d& A1,
                                   const doublearray2d& A2);
    
    friend doublearray2d operator*(const double&        a1,
                                   const doublearray2d& A2);
    
    friend doublearray2d operator*(const doublearray2d& A1,
                                   const double&        a2);
	
	void setIndex1Begin(long i);
	long getIndex1Begin() const;
	long getIndex1End() const;
	long getIndex1Size() const;
	
	void setIndex2Begin(long i);
	long getIndex2Begin() const;
	long getIndex2End() const;
	long getIndex2Size() const;
	
	void operator=(const doublearray2d& d);
	
	void setToValue(double d);
	void addValue(double d);
    void save_to_file(const string folder,
                      const string file);
};

#endif /* defined(__First_order_streamer__Array__) */
