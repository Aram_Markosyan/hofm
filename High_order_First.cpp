//
//  High_order.cpp
//  High_order_fluid_model
//
//  Created by Aram Markosyan on 6/15/13.
//  Copyright (c) 2013 Aram Markosyan. All rights reserved.
//

#include "High_order.h"


void High_order::Flux(doublearray2d& flux)
{
    // Flux function
    for (int i = 1; i < grid; i++) {
        // continuity, -upwind
        flux(0,i) = N(0,i) * 0.5 * (N(2,i-1) + N(2,i));
        
        // ions, 0.0
        flux(1,i) = 0.0;
        
        // momentum balance, upwind
        flux(2,i) = 2.0/(3.0*m) * N(0,i-1) * 0.5 * (N(3,i-1) + N(3,i));
        
        // energy balance, -upwind
        flux(3,i) = N(0,i) * 0.5 * (N(4,i-1) + N(4,i));
        
        // energy flux balance, upwind
        flux(4,i) = beta*2.0/(3.0*m) * N(0,i-1) * pow(0.5*(N(3,i-1) + N(3,i)),2.0);
    }
    
    // left boundary fluxes
    flux(0,0) = N(0,0) * N(2,0); flux(1,0) = 0.0;
    flux(2,0) = 2.0/(3.0*m) * N(0,0) * N(3,0);
    flux(3,0) = N(0,0) * N(4,0);
    flux(4,0) = beta*2.0/(3.0*m) * N(0,0) * pow(N(3,0),2.0);
    
    // right boundary fluxes
    flux(0,grid) = N(0,grid - 1) * N(2,grid - 1); flux(1,grid) = 0.0;
    flux(2,grid) = 2.0/(3.0*m) * N(0,grid - 1) * N(3,grid - 1);
    flux(3,grid) = N(0,grid - 1) * N(4,grid - 1);
    flux(4,grid) = beta*2.0/(3.0*m) * N(0,grid - 1) * pow(N(3,grid - 1),2.0);
}


void High_order::RK4(const doublearray2d &u,
                     doublearray2d &K)
{
    F_LF.setToValue(0.0);
    F_RI.setToValue(0.0);
    
    
    doublearray2d flux(5, grid + 1);
    
    flux.setToValue(0.0);
    UtoN(u, N);
    Flux(flux);
    
    
    for (int i = 0; i < grid; i++) {
        if (attachement)
            K(0,i) = -(flux(0,i+1) - flux(0,i))/dx + u(0,i)*(nuI(abs(N(3,i))) + nuA(abs(N(3,i))));
        else
            K(0,i) = -(flux(0,i+1) - flux(0,i))/dx + u(0,i)*nuI(abs(N(3,i)));
        
        K(1,i) = u(0,i)*nuI(abs(N(3,i)));
        
        K(2,i) = -(flux(2,i+1) - flux(2,i))/dx + electron/m * u(0,i)*0.5*(E(i) + E(i-1)) - nuM(abs(N(3,i)))* u(2,i);
        
        K(3,i) = -(flux(3,i+1) - flux(3,i))/dx + electron*0.5*(E(i) + E(i-1))*u(2,i) - nuE(abs(N(3,i)))*u(3,i) - u(0,i)*(-1.5*k*nuE(abs(N(3,i)))*T_0
                  + epsI*nuI(abs(N(3,i))) + nuTot(abs(N(3,i))) );
        
        K(4,i) = -(flux(4,i+1) - flux(4,i))/dx + 5.0/3.0 * electron/m * 0.5*(E(i) + E(i-1)) * u(3,i) - nuM(abs(N(3,i)))*u(4,i);
    }
}

// Run baby, run!
void High_order::Propagate()
{
    doublearray2d K_1(5,grid), K_2(5,grid),
    K_3(5,grid), K_4(5,grid);
    while (t < final_time) {
        Dt(N);
        for (int i = 0; i<grid; i++) {
          if (N(3,i) < 0) {
            cout << "Negative density encountered :(" << endl;
                grid = i - 10;
            //exit(1);
          }
        }

        K_1.setToValue(0.0);
        K_2.setToValue(0.0);
        K_3.setToValue(0.0);
        K_4.setToValue(0.0);

        RK4(U, K_1);
        Update_field(U);
        //UtoN(U, N);

        RK4(U + 0.5*dt*K_1, K_2);
        Update_field(U + 0.5*dt*K_1);
        //UtoN(U + 0.5*dt*K_1, N);

        RK4(U + 0.5*dt*K_2, K_3);
        Update_field(U + 0.5*dt*K_2);
        //UtoN(U + 0.5*dt*K_2, N);

        RK4(U + dt*K_3, K_4);
        U = U + 1.0/6.0 * dt * (K_1 + 2.0*K_2 + 2.0*K_3 + K_4);

        // for (int i = 0; i < grid; i++) {
        //   if (U(2,i) > 0) U(2,i) = 0.0;
        // }

        Update_field(U);
        t = t + dt;
        general_step++;
        UtoN(U, N);
//        if (general_step%200 == 0) {
//          cout << "Output number " << general_step << " written with dt "<<dt<<"\n";
//          Output();
//        }
    }
}
